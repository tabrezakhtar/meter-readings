# Bulb Meter Readings

### Completed by Tabrez Akhtar


## How to run
To install dependencies:   
```npm install```  

Run unit tests:  
```npm test```

View Code Coverage:  
Open `/coverage/lcov-report/index.html` in the browser

#### Question1

To get the largest increase, I use the reduce command to keep track of the previous values.  The size of the increase was calculated for each reading then compared to the previous.  If it was greater, then the current reading was added to the accumulator.

#### Question2

To estimate readings, I first filled in the missing months.  This was done by looping through the readings and checking if the next month was available: `if (currentMonth.getMonth() + 1 !== nextMonth.getMonth())`
If not, then I added a month with the correct date.

The readings were estimated by adding the previous months reading to the next months reading, then dividing by 2.  There are some checks to see if the previous/next month exist (may be beginning or end of array, or missing month).  The end of month date is generated using the date-fns library.

This function could be improved by combining the loops together to make it more efficient.