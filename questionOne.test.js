const {questionOne} = require('./solutions');

describe('find largest energy increase', () => {
  describe('when an array of meter readings is passed in', () => {
    it('should find the largest reading', () => {
      const exampleReadings = [
        {
          "cumulative": 10270,
          "readingDate": "2019-06-18T00:00:00.000Z"
        },
        {
          "cumulative": 18453,
          "readingDate": "2019-07-31T00:00:00.000Z"
        },
        {
          "cumulative": 19620,
          "readingDate": "2019-08-31T00:00:00.000Z"
        },
        {
          "cumulative": 79682,
          "readingDate": "2019-09-10T00:00:00.000Z"
        }
      ];

      expect(questionOne(exampleReadings)).toEqual(exampleReadings[3]);
    });
  });

  describe('when an array of meter readings is passedin where some are the same', () => {
    it('should find the first matching', () => {
      const exampleReadings = [
        {
          "cumulative": 10270,
          "readingDate": "2019-06-18T00:00:00.000Z"
        },
        {
          "cumulative": 18453,
          "readingDate": "2019-07-31T00:00:00.000Z"
        },
        {
          "cumulative": 18453,
          "readingDate": "2019-08-31T00:00:00.000Z"
        },
        {
          "cumulative": 19736,
          "readingDate": "2019-09-10T00:00:00.000Z"
        }
      ];

      expect(questionOne(exampleReadings)).toEqual(exampleReadings[1]);
    });
  });

  describe('when an array of meter readings is passed where all are same', () => {
    it('should find the first matching', () => {
      const exampleReadings = [
        {
          "cumulative": 10270,
          "readingDate": "2019-06-18T00:00:00.000Z"
        },
        {
          "cumulative": 10270,
          "readingDate": "2019-07-31T00:00:00.000Z"
        },
        {
          "cumulative": 10270,
          "readingDate": "2019-08-31T00:00:00.000Z"
        },
        {
          "cumulative": 10270,
          "readingDate": "2019-09-10T00:00:00.000Z"
        }
      ];

      expect(questionOne(exampleReadings)).toEqual(exampleReadings[0]);
    });
  });

  describe('when no array passed in', () => {
    it('should find the first matching', () => {
      expect(questionOne()).toEqual(null);
    });
  });
});