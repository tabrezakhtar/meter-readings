const {addMonths, endOfMonth} = require('date-fns');
const {questionTwo} = require('./solutions');

describe('estimate readings', () => {
  describe('when an array of meter readings is passed in', () => {
    it('should estimate the readings', () => {
      const readings = [
        {
          "cumulative": 17580,
          "readingDate": "2019-03-28T00:00:00.000Z"
        },
        {
          "cumulative": 17759,
          "readingDate": "2019-04-15T00:00:00.000Z"
        },
        {
          "cumulative": 18002,
          "readingDate": "2019-05-08T00:00:00.000Z"
        }
      ]

      expect(questionTwo(readings)).toEqual([
        {
          estimatedRead: 17580,
          date: endOfMonth(new Date(readings[0].readingDate))
        },
        {
          estimatedRead: 17791,
          date: endOfMonth(new Date(readings[1].readingDate))
        },
      ]);
    });
  });

  describe('when only 1 reading is passed in', () => {
    it('should not estimate', () => {
      const readings = [
        {
          "cumulative": 17580,
          "readingDate": "2019-03-28T00:00:00.000Z"
        }
      ]

      expect(questionTwo(readings)).toEqual([]);
    });
  });

  describe('when an array of meter readings is passed in with a missing month', () => {
    it('should estimate the readings', () => {
      const readings = [
        {
          "cumulative": 17580,
          "readingDate": "2019-03-28T00:00:00.000Z"
        },
        {
          "cumulative": 17759,
          "readingDate": "2019-04-15T00:00:00.000Z"
        },
        {
          "cumulative": 18002,
          "readingDate": "2019-05-08T00:00:00.000Z"
        },
        {
          "cumulative": 18453,
          "readingDate": "2019-07-31T00:00:00.000Z"
        },
        {
          "cumulative": 18620,
          "readingDate": "2019-08-31T00:00:00.000Z"
        }
      ]

      expect(questionTwo(readings)).toEqual([
        {
          estimatedRead: 17580,
          date: endOfMonth(new Date(readings[0].readingDate))
        },
        {
          estimatedRead: 17791,
          date: endOfMonth(new Date(readings[1].readingDate))
        },
        {
          estimatedRead: 18002,
          date: endOfMonth(new Date(readings[2].readingDate))
        },
        {
          estimatedRead: 18227,
          date: endOfMonth(addMonths(new Date(readings[2].readingDate), 1))
        },
        {
          estimatedRead: 18453,
          date: endOfMonth(new Date(readings[3].readingDate))
        }
      ]);
    });
  });
});