const {addMonths, endOfMonth} = require('date-fns');

function questionOne(readings) {

  if (!readings || !readings.length) {
    return null;
  }

  const biggestIncrease = readings.reduce( (previous, current) => {
    const increase = current.cumulative - previous.cumulative;

    if (increase > previous.increase) {
      return { ...current, increase, biggestIncreaseObject: current };
    }

    return { ...current, increase: previous.increase, biggestIncreaseObject: previous.biggestIncreaseObject };
  }, {...readings[0], increase: 0, biggestIncreaseObject: readings[0]});

  return biggestIncrease.biggestIncreaseObject;
}

function fillMissingMonths(readings) {
  const months = [];
  
  for (let index = 0; index < readings.length; index++) {
    if (index + 1 >= readings.length) {
      months.push(readings[index]);
      continue;
    }

    const currentMonth = new Date(readings[index].readingDate);
    const nextMonth = new Date(readings[index + 1].readingDate);

    months.push(readings[index]);

    //check for missing date
    if (currentMonth.getMonth() + 1 !== nextMonth.getMonth()) {
      months.push({"cumulative": 0, "readingDate": addMonths(currentMonth, 1).toISOString()});
    }
  }
  
  return months;
}

function questionTwo(readings) {
  readings = fillMissingMonths(readings);

  const estimatedReadings = [];
  for (let index = 0; index < readings.length - 1; index++) {
    const previousMonthReading = index > 0 ? readings[index - 1].cumulative : 0;
    const currentMonthReading = readings[index].cumulative;
    const nextMonthReading = readings[index + 1].cumulative;

    const estimatedReading = previousMonthReading === 0 || nextMonthReading === 0 ? currentMonthReading : (previousMonthReading + nextMonthReading) / 2;
    estimatedReadings.push({estimatedRead: Math.floor(estimatedReading), date: endOfMonth(new Date(readings[index].readingDate))})
  }

  return estimatedReadings;
}

module.exports = {questionOne, questionTwo}